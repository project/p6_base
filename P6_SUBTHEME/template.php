<?php
/**
 * @file
 */

/**
 * Adds a default set of helper variables for variable processors and templates.
 */
// function P6_SUBTHEME_preprocess(&$vars, $hook) {
// }

/**
 * Preprocess variables for html.tpl.php
 */
// function P6_SUBTHEME_preprocess_html(&$vars) {
// }

/**
 * Preprocess variables for page.tpl.php
 */
// function P6_SUBTHEME_preprocess_page(&$vars) {
// }

/**
 * Maintenance page preprocessing
 */
// function P6_SUBTHEME_preprocess_maintenance_page(&$vars) {
//   P6_SUBTHEME_preprocess_page($vars);
// }

/**
 * Process variables for node.tpl.php
 *
 * @see node.tpl.php
 */
// function P6_SUBTHEME_preprocess_node(&$vars) {
// }

/**
 * Processes variables for block.tpl.php.
 *
 * @see block.tpl.php
 */
// function P6_SUBTHEME_preprocess_block(&$vars) {
// }

/**
 * Process variables for block.tpl.php.
 *
 * @see block.tpl.php
 */
// function P6_SUBTHEME_process_block(&$vars) {
// }

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
// function P6_SUBTHEME_breadcrumb($vars) {
// }
